let num1 = parseFloat(prompt("Введіть перше число:"));
let num2 = parseFloat(prompt("Введіть друге число:"));
let operation = prompt("Введіть математичну операцію (+, -, *, /):");

console.log(calculate(num1, num2, operation));

function calculate(n1, n2, op) {
  let result;
  switch (op) {
    case "+":
      result = n1 + n2;
      break;
    case "-":
      result = n1 - n2;
      break;
    case "*":
      result = n1 * n2;
      break;
    case "/":
      result = n1 / n2;
      break;
    default:
      console.log("Невірна операція");
      return;
  }

  return result;
}
